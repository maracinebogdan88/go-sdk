	package request
	import (


		"github.com/maracinebogdan88/go-sdk/HttpClients"

		"github.com/maracinebogdan88/go-sdk/Base"

		"github.com/maracinebogdan88/go-sdk/NeokamiResponse"

		"log"

		"github.com/maracinebogdan88/go-sdk/Exceptions"

		"strings"

	)

	type NeokamiRequest struct {

		base.Base
		wait, max_retries int
		sleep float64
		output_format, output_type, apiKey string
		silentFails bool
	}

	func NewNeokamiRequest() *NeokamiRequest {

		p := new(NeokamiRequest)
		p.wait = 1
		p.sleep = 1
		p.max_retries = 5
		p.apiKey=""
		p.output_type=""
		p.output_format=""
		p.silentFails=false

		return p
	}

	func (p *NeokamiRequest) SetOutputFormat(output_format string){

		p.output_format=output_format
	}

	func (i NeokamiRequest) GetSleep() float64 {

		return i.sleep;
	}

	func (p *NeokamiRequest) SetSleep(sleep float64){

		p.sleep=sleep
	}

	func (i NeokamiRequest) GetOutputFormat() string {
		return i.output_format;
	}

	func (p *NeokamiRequest) SetMaxRetries (max_retries int){

		p.max_retries=max_retries
	}

	func (i NeokamiRequest) GetMaxRetries() int{

	return i.max_retries

	}
	func (p *NeokamiRequest) SetWait (wait int){

		p.wait=wait
	}

	func (i NeokamiRequest) GetWait() int{

		return i.wait

	}
	func (p *NeokamiRequest) SetApiKey (apiKey string){

		p.apiKey=apiKey
	}

	func (i NeokamiRequest) GetApiKey() string{

		return i.apiKey;

	}
	func (p *NeokamiRequest) SetSilentFails (silentFails bool){

		p.silentFails=silentFails
	}

	func (i NeokamiRequest) GetSilentFails() bool{

		return i.silentFails

	}
	func (p *NeokamiRequest) SetOutputType (output_type string){

		valid:=[]string{"memory", "rabbitmq"}

		if !p.contains(valid,output_type){

			msg := []string{"Specified output is not valid. Valid types are "}
			ex:=exceptions.NewNeokamiParametersException(strings.Join(append(msg,valid...), " "))
			log.Panicln(ex.GetMessage())
		}
		p.output_type=output_type;
	}

	func (i NeokamiRequest) contains(slice []string, item string) bool {
		set := make(map[string]struct{}, len(slice))
		for _, s := range slice {
			set[s] = struct{}{}
		}

		_, ok := set[item]
		return ok
	}

	func (i NeokamiRequest) GetOutputType() string{

		return i.output_type;
	}
	func (i NeokamiRequest) CheckFilepath( file_path string) bool{

		if len(file_path)==0 || file_path=="" {

			ex:=exceptions.NewNeokamiParametersException("Missing Filepath ")
			log.Panicln(ex.GetMessage())
		}
		return true
	}

	func (i NeokamiRequest) CheckInputStream( file_stream []byte) bool{

		if len(file_stream)==0{
			
			ex:=exceptions.NewNeokamiParametersException("No compatible data found, try analyzing a file from disk instead.")
			log.Panicln(ex.GetMessage())
		}
		return true;
	}

	func (i NeokamiRequest) CheckHasAllParameters( payload map[string]string) bool{

		for key, value := range payload {

			if  value =="" ||len(value)==0 {

				ex:=exceptions.NewNeokamiParametersException("Missing parameter "+key)
				log.Panicln(ex.GetMessage())
			}
		}
		return true;
	}

	func (i NeokamiRequest) GetResult(jobId string) *response.NeokamiResponse{

		var jsonResponse []byte
		neokamiCurl:=httpClients.NeokamiCurl{}

		m:=make(map[string]string)
		m["job_id"]=jobId;
		m["api_key"]=i.apiKey;
		m["sdk_version"]=base.SDK_VERSION;
		m["sdk_lang"]=base.SDK_LANG;

		jsonResponse=neokamiCurl.Post("https://www.neokami.io/engine/job/results",m)

		return response.NewNeokamiResponse(jsonResponse,i.GetOutputFormat(),i.GetSilentFails())
	}
