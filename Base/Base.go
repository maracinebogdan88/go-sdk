package base

type  Base struct{}

const (

	API_BASE string = "https://testing.neokami.io";
	SDK_VERSION string = "0.1";
	SDK_LANG string = "php";
)

func (inside Base) GetUrl(path string ) string {

return API_BASE+path;

}
