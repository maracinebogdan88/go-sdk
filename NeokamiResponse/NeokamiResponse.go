package response
import (
	"encoding/json"
	"reflect"
	"github.com/maracinebogdan88/go-sdk/Exceptions"
	"log"
)

type NeokamiResponse struct{
	
	jsonResponse []byte
	jsonResponseObj map[string]interface{}
	silentFails bool
	outputFormat string
}

func NewNeokamiResponse(jsonResponse []byte,outputFormat string,silentFails bool) *NeokamiResponse{

	p := new(NeokamiResponse)
	p.outputFormat=outputFormat;
	p.jsonResponse=jsonResponse;
	p.silentFails=silentFails
	p.jsonResponseObj=p.UnmarshallJSONResponse(jsonResponse);
	p.Validate();

	return p;
}


func (p *NeokamiResponse) UnmarshallJSONResponse (jsonResponse []byte) map[string]interface{}{

	var jsonResponseObj map[string]interface{}

	if err := json.Unmarshal([]byte(jsonResponse), &jsonResponseObj); err != nil {

		ex:=exceptions.NewNeokamiServerException(nil,503)
		log.Panicln(ex.GetMessage(),ex.GetStatusCode())
	}

	return jsonResponseObj
}

func (i NeokamiResponse) Status() int {
	status, _ := i.jsonResponseObj["status_code"].(float64)
	return int(status)
}

func (i NeokamiResponse) Warnings() (interface{}){

	return i.jsonResponseObj["warnings"]

}

func (i NeokamiResponse) Retries() int{

	if i.Warnings()!=nil  {

		v := i.CheckTypeMap(i.Warnings())

		if v {
			retries, _ := i.jsonResponseObj["warnings"].(map[string]interface{})["retries"].(float64)
			return int(retries)
		}

	}
	return 0;
}

func(i NeokamiResponse) HasError() bool{

	errors, _ := i.jsonResponseObj["errors"]
	if errors!=nil{
		return true;
	}

	return false;
}

func (i NeokamiResponse) Errors() interface{}{

	if(i.HasError()) {
		return i.jsonResponseObj["errors"];
	}
	return nil

}
func (i NeokamiResponse) Result() interface{}{

	result, _ := i.jsonResponseObj["result"]
	return result;
}
func (i NeokamiResponse) CheckTypeMap(obj interface{}) bool{

	v := reflect.ValueOf(obj)
	if v.Kind() == reflect.Map {
		return true
	} else {
		return false
	}
}

func (i NeokamiResponse) Validate() bool{

	if i.silentFails==true{

		return true;
	}
	switch(i.Status()){

	case 400:
		ex:=exceptions.NewNeokamiSDKException(i.jsonResponseObj,400)
		log.Panicln(ex.GetMessage(),ex.GetStatusCode())
	case 401:
		ex:=exceptions.NewNeokamiAuthorizationException(i.jsonResponseObj,401)
		log.Panicln(ex.GetMessage(),ex.GetStatusCode())
	case 402:
		ex:=exceptions.NewNeokamiBlockedException(i.jsonResponseObj,402)
		log.Panicln(ex.GetMessage(),ex.GetStatusCode())
	case 403:
		ex:=exceptions.NewNeokamiBlockedException(i.jsonResponseObj,403)
		log.Panicln(ex.GetMessage(),ex.GetStatusCode())
	case 426:
		ex:=exceptions.NewNeokamiSDKException(i.jsonResponseObj,426)
		log.Panicln(ex.GetMessage(),ex.GetStatusCode())
	case 500:
		ex:=exceptions.NewNeokamiServerException(i.jsonResponseObj,500)
		log.Panicln(ex.GetMessage(),ex.GetStatusCode())
	default:
		return true;

	}
	return true
}
