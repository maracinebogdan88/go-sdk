package sentiment
import (
	"github.com/maracinebogdan88/go-sdk/NeokamiRequest"

	"github.com/maracinebogdan88/go-sdk/NeokamiResponse"
	"strconv"
	"github.com/maracinebogdan88/go-sdk/Base"
	"github.com/maracinebogdan88/go-sdk/HttpClients"
)

type SentimentAnalyser struct {

	*request.NeokamiRequest
	sentences int
	text string
}

func NewSentimentAnalyser() *SentimentAnalyser{

	s:=new(SentimentAnalyser)
	s.NeokamiRequest=request.NewNeokamiRequest()
	return s
}

func (i *SentimentAnalyser) Analyse() *response.NeokamiResponse{

	m:=make(map[string]string)
	m["wait"]=strconv.Itoa(i.GetWait())
	m["max_retries"]=strconv.Itoa(i.GetMaxRetries())
	m["sleep"]=strconv.FormatFloat(i.GetSleep(),'f', 2, 64)
	m["api_key"]=i.GetApiKey();
	m["sdk_version"]=base.SDK_VERSION
	m["sdk_lang"]=base.SDK_LANG
	m["sentences"]=strconv.Itoa(i.GetSplitText())
	m["text"]=i.GetText()

	var jsonResponse []byte
	nc:=httpClients.NeokamiCurl{}

	i.CheckHasAllParameters(m)
	jsonResponse=nc.Post(i.GetUrl("/analyse/text/sentiment"),m)

	return response.NewNeokamiResponse(jsonResponse,i.GetOutputFormat(),i.GetSilentFails())
}

func (p *SentimentAnalyser) SetSplitText(sentences int){

	p.sentences=sentences
}

func (i SentimentAnalyser) GetSplitText() int{

	return i.sentences
}

func (p *SentimentAnalyser) SetText(text string){

	p.text=text
}

func (i SentimentAnalyser) GetText() string{

	return i.text
}