package exceptions

type Exception struct{

	message interface{}
}

func NewException(message interface{}) *Exception{

	p:=new(Exception)
	p.message=message

	return p
}

func (i Exception) GetMessage() interface{}{

	return i.message
}

type NeokamiParametersException struct{

	*Exception
}

func NewNeokamiParametersException(message interface{}) *NeokamiParametersException{

	p:=new(NeokamiParametersException);
	p.Exception=NewException(message)

	return p
}

type NeokamiBaseException struct{

	*Exception
	jsonResponseObj map[string]interface{}
	code int
}

func NewNeokamiBaseException(jsonResponseObj map[string]interface{},code int) *NeokamiBaseException{

	p:=new(NeokamiBaseException)
	p.jsonResponseObj=jsonResponseObj
	p.Exception=NewException(p.CheckJsonObject(jsonResponseObj))
	p.code=code

	return p

}

func (p *NeokamiBaseException) CheckJsonObject(jsonResponseObj map[string]interface{}) interface{}{

	var message interface{}="Malformed server response, please try again later."

	if len(jsonResponseObj)>0 {

		err := p.jsonResponseObj["errors"]
		if err==nil {
			message=nil
		}else {
			message=err
		}
	}

	return message

}

func (i NeokamiBaseException) GetStatusCode() int{

	return i.code
}

func (i NeokamiBaseException) IsMalformed() bool{

	if i.jsonResponseObj==nil || len(i.jsonResponseObj)==0{
		return true
	}
	return false
}
func (i NeokamiBaseException) GetWarnings() interface{}{

	return i.jsonResponseObj["warnings"]
}
func (i NeokamiBaseException) GetError() interface{}{

	if !i.IsMalformed(){
		errors, _ := i.jsonResponseObj["errors"]
		return errors
	}
	return nil
}

type NeokamiSDKException struct {

	*NeokamiBaseException
}

func NewNeokamiSDKException(jsonResponseObj map[string]interface{},code int) *NeokamiSDKException{

	p:=new(NeokamiSDKException)
	p.NeokamiBaseException=NewNeokamiBaseException(jsonResponseObj,code)
	return p
}

type NeokamiAuthorizationException struct {

	*NeokamiBaseException
}

func NewNeokamiAuthorizationException(jsonResponseObj map[string]interface{},code int) *NeokamiAuthorizationException{

	p:=new(NeokamiAuthorizationException)
	p.NeokamiBaseException=NewNeokamiBaseException(jsonResponseObj,code)
	return p
}
type NeokamiServerException struct {

	*NeokamiBaseException
}

func NewNeokamiServerException(jsonResponseObj map[string]interface{},code int) *NeokamiServerException{

	p:=new(NeokamiServerException)
	p.NeokamiBaseException=NewNeokamiBaseException(jsonResponseObj,code)
	return p
}
type NeokamiBlockedException struct {

	*NeokamiBaseException
}

func NewNeokamiBlockedException(jsonResponseObj map[string]interface{},code int) *NeokamiBlockedException{

	p:=new(NeokamiBlockedException)
	p.NeokamiBaseException=NewNeokamiBaseException(jsonResponseObj,code)
	return p
}
