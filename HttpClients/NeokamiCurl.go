	package httpClients

	import (
		"fmt"
		"bytes"
		"net/http"
		"io/ioutil"
		"encoding/json"
		"os"
		"mime/multipart"
		"io"
	)

	type NeokamiCurl struct{}

	func (inside NeokamiCurl) Get(url string) []byte{

		req, err := http.NewRequest("GET", url,nil)
		req.Header.Set("Content-Type", "application/json")

		client := &http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			panic(err)
		}
		defer resp.Body.Close()

		fmt.Println("response Status:", resp.Status)
		body, _ := ioutil.ReadAll(resp.Body)
		fmt.Println("response Body:", string(body))

		return body
	}

	func (inside NeokamiCurl) Post(url string, payload map[string]string) []byte{

		jsonStr, _ := json.Marshal(payload)
		req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
		req.Header.Set("Content-Type", "application/json")
		if apikey, ok := payload["api_key"]; ok {
			req.Header.Set("apikey",apikey)
		}
		client := &http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			panic(err)
		}
		defer resp.Body.Close()

		fmt.Println("response Status:", resp.Status)
		body, _ := ioutil.ReadAll(resp.Body)
		fmt.Println("response Body:", string(body))

		return body
	}

	func (inside NeokamiCurl) PostBinaryPath(request_url string,file_path string, payload map[string]string ) []byte{

		client := &http.Client{}

		body := &bytes.Buffer{}

		body_writer := multipart.NewWriter(body)

		for field_name, field_value := range payload {
			body_writer.WriteField(field_name, field_value)
		}

		file, err := os.Open(file_path)
		if err != nil {
			panic(err)
		}
		fi, err := file.Stat()
		if err != nil {
			panic(err)
		}
		file_writer, err := body_writer.CreateFormFile("data", fi.Name())
		if err != nil {
			panic(err)
		}

		_, err = io.Copy(file_writer, file)
		if err != nil {
			panic(err)
		}

		file.Close()

		body_writer.Close()

		content_type := body_writer.FormDataContentType()

		req, err := http.NewRequest("POST", request_url, body)
		req.Header.Set("Content-Type", content_type)

		if apikey, ok := payload["api_key"]; ok {
			req.Header.Set("apikey",apikey)
		}

		rsp, err := client.Do(req)
		if err != nil {
			panic(err)
		}
		body_byte, err := ioutil.ReadAll(rsp.Body)
		if err != nil {
			panic(err)
		}
		rsp.Body.Close()
		fmt.Println(string(body_byte))

		return body_byte

	}

	func (inside NeokamiCurl) PostBinaryByte(request_url string,input_stream []byte, payload map[string]string ) []byte{

		client := &http.Client{}

		body := &bytes.Buffer{}

		body_writer := multipart.NewWriter(body)

		for field_name, field_value := range payload {
			body_writer.WriteField(field_name, field_value)
		}

		file_writer, err := body_writer.CreateFormFile("data","image as byte array")

		file_writer.Write(input_stream)

		body_writer.Close()

		content_type := body_writer.FormDataContentType()

		req, err := http.NewRequest("POST", request_url, body)
		req.Header.Set("Content-Type", content_type)

		if apikey, ok := payload["api_key"]; ok {
			req.Header.Set("apikey",apikey)
		}
		rsp, err := client.Do(req)
		if err != nil {
			panic(err)
		}
		body_byte, err := ioutil.ReadAll(rsp.Body)
		if err != nil {
			panic(err)
		}
		rsp.Body.Close()
		fmt.Println(string(body_byte))

		return body_byte

	}
