package analyser
import (
	"strconv"
	"github.com/maracinebogdan88/go-sdk/HttpClients"
	"github.com/maracinebogdan88/go-sdk/NeokamiResponse"
	"github.com/maracinebogdan88/go-sdk/NeokamiRequest"
	"github.com/maracinebogdan88/go-sdk/Base"
)

type ImageAnalyser struct {

	*request.NeokamiRequest
	file_stream []byte
	file_path string
}
func NewImageAnalyser() *ImageAnalyser {

	im:=new(ImageAnalyser)
	im.NeokamiRequest=request.NewNeokamiRequest()
	return im;
}

func (inside ImageAnalyser) Analyze() *response.NeokamiResponse{

	if len(inside.GetInputStream())>0 {
		return inside.AnalyseFromInputStream()
	}
	return inside.AnalyseFromDisk()

}

func (i ImageAnalyser) AnalyseFromDisk() *response.NeokamiResponse{

	m:=make(map[string]string)
	m["wait"]=strconv.Itoa(i.GetWait())
	m["max_retries"]=strconv.Itoa(i.GetMaxRetries())
	m["sleep"]=strconv.FormatFloat(i.GetSleep(),'f', 2, 64)
	m["api_key"]=i.GetApiKey()
	m["sdk_version"]=base.SDK_VERSION
	m["sdk_lang"]=base.SDK_LANG

	var jsonResponse []byte
	nc:=httpClients.NeokamiCurl{}

	i.CheckFilepath(i.GetFilePath())
	i.CheckHasAllParameters(m)
	jsonResponse=nc.PostBinaryPath(i.GetUrl("/analyse/image"),i.GetFilePath(),m)

	return response.NewNeokamiResponse(jsonResponse,i.GetOutputFormat(),i.GetSilentFails())
}

func (i ImageAnalyser) AnalyseFromInputStream() *response.NeokamiResponse{

	nc:=httpClients.NeokamiCurl{}
	m:=make(map[string]string)
	m["wait"]=strconv.Itoa(i.GetWait());
	m["max_retries"]=strconv.Itoa(i.GetMaxRetries());
	m["sleep"]=strconv.FormatFloat(i.GetSleep(),'f', 2, 64);
	m["api_key"]=i.GetApiKey();
	m["sdk_version"]=base.SDK_VERSION;
	m["sdk_lang"]=base.SDK_LANG;

	var jsonResponse []byte

	i.CheckInputStream(i.GetInputStream())
	i.CheckHasAllParameters(m)
	jsonResponse=nc.PostBinaryByte(i.GetUrl("/analyse/image"),i.GetInputStream(),m)

	return response.NewNeokamiResponse(jsonResponse,i.GetOutputFormat(),i.GetSilentFails())
}

func (p *ImageAnalyser) SetInputStream(file_stream []byte) {

	p.file_stream=file_stream
}

func (i ImageAnalyser) GetInputStream() []byte {

	return i.file_stream
}

func (p *ImageAnalyser) SetFilePath(file_path string) {

	p.file_path=file_path
}

func (i ImageAnalyser) GetFilePath() string{

	return i.file_path
}

